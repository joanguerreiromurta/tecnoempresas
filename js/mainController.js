app.controller ('mainController' , mainController)

function mainController($scope, factoryJson){

	var vm = $scope;

	var restful = factoryJson;

	vm.mostrar = mostrar;

	vm.valor = false;

	vm.mostrando = [];

	vm.clientes = [];

	vm.productos = [];	

	///////////////////////////////////////////////////

	function mostrar(valor){
		switch (valor){

			case 0:
				vm.mostrando[0] = true;
				vm.mostrando[1] = false;
				vm.mostrando[2] = false;
				break;
			case 1:
				vm.mostrando[0] = false;
				vm.mostrando[1] = true;
				vm.mostrando[2] = false;
				break;
			case 2:
				vm.mostrando[0] = false;
				vm.mostrando[1] = false;
				vm.mostrando[2] = true;
				break;
		}

	}

	///////////////////////////////////////////////////

	vm.traerCompanias = traerCompanias;

	function traerCompanias(){

		restful.getJson('companies.json').success(function(companies){

			vm.companias = companies;

		})
	}

	vm.traerCompanias();

	///////////////////////////////////////////////////

	vm.traerClientesCompania = traerClientesCompania;

	function traerClientesCompania(valor,index){

		switch (valor){

			case '0001':
				restful.getJson('clients0001.json').success(function(clients){

					vm.clientes[0] = clients;									

				})
				break;
			case '0002':
				restful.getJson('clients0002.json').success(function(clients){

					vm.clientes[1] = clients;

				})
				break;
			case '0003':
				restful.getJson('clients0003.json').success(function(clients){

					vm.clientes[2] = clients;
				})
				break;
		}

		
	}

	///////////////////////////////////////////////////


	vm.traerProductosCompania = traerProductosCompania;

	function traerProductosCompania(valor,index){

		switch (valor){

			case '0001':
				restful.getJson('products0001.json').success(function(products){

					vm.productos[0] = products;									

				})
				break;
			case '0002':
				restful.getJson('products0002.json').success(function(products){

					vm.productos[1] = products;

				})
				break;
			case '0003':
				restful.getJson('products0003.json').success(function(products){

					vm.productos[2] = products;					
				})
				break;
		}

		
	}

	///////////////////////////////////////////////////
	
	var indexedCompanias = [];

	vm.companiasToFilter = function() {
		indexedCompanias = [];
		return vm.companias.result.companies;
	}

	vm.filtrarCompanias = function(compania) {
		var companiaEncontrada = indexedCompanias.indexOf(compania.name) == -1;
		if (companiaEncontrada) {
			indexedCompanias.push(compania.name);
		}
		return companiaEncontrada;
	}

}