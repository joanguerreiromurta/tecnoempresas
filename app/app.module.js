var app = angular.module('tecnoEmpresas', ['ngRoute']);

app.config(function($routeProvider) {

	$routeProvider.when("/", {
        templateUrl: "views/home.html",
        controller: 'mainController'
    })

	$routeProvider.otherwise({
	      redirectTo: '/'
	}); 
 
});