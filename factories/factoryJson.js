app.factory("factoryJson",function ($http){

  var Service = {
    getJson : getJson
  };

  return Service;

  function getJson(json){
    var url = 'dataJson/' + json;
    return $http.get(url);
  };

});
